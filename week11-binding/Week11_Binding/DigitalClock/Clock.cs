﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using DigitalClock.Annotations;

namespace DigitalClock
{
    public class Clock : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool SetProperty<T>(ref T field, T value, [CallerMemberName] string formattedTime = null)
        {
            if (Equals(field, value))
            {
                return false;
            }
            field = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(formattedTime));
            return true;
        }

        private DispatcherTimer _timer;
        private DateTime _currentDateTime;

        public DateTime CurreDateTime
        {
            get { return _currentDateTime; }
            set { SetProperty(ref _currentDateTime, value); }
        }

        private String _formattedTime;

        public String FormattedTime
        {
            get
            {
                return CurreDateTime.ToLongTimeString();
            }
            set { SetProperty(ref _formattedTime, value); }
        }

        public Clock()
        {
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += timer_Tick;
            _timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            CurreDateTime = DateTime.Now;
            FormattedTime = CurreDateTime.ToLongTimeString();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
