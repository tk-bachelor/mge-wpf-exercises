﻿using System.Windows;

namespace DigitalClock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public Clock MyClock { get; set; }
        
        public MainWindow()
        {
            InitializeComponent();
            MyClock = new Clock();
            this.DataContext = this;
        }
    }
}
