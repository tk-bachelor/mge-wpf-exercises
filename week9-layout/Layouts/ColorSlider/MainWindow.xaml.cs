﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ColorSlider
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            UpdateColorValues(0,0,0);
        }

        private void ColorChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            byte r = (byte)RedSlider.Value;
            byte g = (byte)GreenSlider.Value;
            byte b = (byte)BlueSlider.Value;

            UpdateColorValues(r, g, b);
        }

        private void UpdateColorValues(byte r, byte g, byte b)
        {
            Color c = Color.FromRgb(r, g, b);

            ColorDisplay.Background = new SolidColorBrush(c);

            RedValue.Text = $"{r}";
            GreenValue.Text = $"{g}";
            BlueValue.Text = $"{b}";

            ColorDisplay.Content = HexConverter(r, g, b);
        }

        private static String HexConverter(byte r, byte g, byte b)
        {
            return "#" + r.ToString("X2") + g.ToString("X2") + b.ToString("X2");
        }
    }
}
