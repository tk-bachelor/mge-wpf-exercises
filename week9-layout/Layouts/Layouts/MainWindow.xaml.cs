﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Layouts
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (LoginName.Text == "max.muster@gmail.com" && Password.Password == "1234")
            {
                Ok.Content = "Success";
                MessageBox.Show(this, "Welldone", "Message");
            }
            else
            {
                Ok.Content = "Error";
                MessageBox.Show(this, "Invalid username and password", "Message");
            }
        }
    }
}
