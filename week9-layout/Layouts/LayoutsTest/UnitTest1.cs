﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;

namespace LayoutsTest
{
    [TestClass]
    public class UnitTest1
    {

        /// <summary>
        /// the directory in which the test is running
        /// </summary>
        public string BaseDir => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// system under test (wpf app to be tested)
        /// </summary>
        public string SutPath => Path.Combine(BaseDir, $"{nameof(Layouts)}.exe");


        [TestMethod]
        public void TestMethod1()
        {
            var app = Application.Launch(SutPath);
            var window = app.GetWindow("Account Settings", InitializeOption.NoCache);

            var name = window.Get<TextBox>("LoginName");
            var pw = window.Get<TextBox>("Password");

            name.Text = "max.muster@gmail.com";
            pw.Text = "1234";

            var okButton = window.Get<Button>("Ok");
            okButton.Click();

            Assert.AreEqual("Success", okButton.Text);
            app.Close();
        }
    }
}
